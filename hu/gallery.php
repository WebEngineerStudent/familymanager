<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Galléria</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
  <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/gallery.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src="<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align="center"><?php echo $_SESSION["usernameSession"]." (".$_SESSION["firstnameSession"]." ".$_SESSION["lastnameSession"].") - ".$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Család</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Napi rutin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="programs.php"><i class="fas fa-child"></i> Családi programok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Feladatok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="gallery.php"><i class="fas fa-images"></i> Családi galléria</a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="chatroom.php"><i class="fas fa-comments"></i> Társalgó</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container col-12 col-lg-8" id="contentDiv">
			<div class="container col-10 col-lg-12" id="galleryHolder">
					<form method="POST" action="php/uploadGalleryImage.php" enctype="multipart/form-data">
						<!-- COMPONENT START -->
						<div class="form-group">
							<div class="input-group input-file" name="Fichier1">
								<input class='form-control' type='file' id='uploadGaleryPicture' name='uploadGaleryPicture' placeholder="Válaszd ki a képet...">
								<span class="input-group-btn">
								<button class="btn btn-primary btn-reset" type="submit"><i class="fas fa-upload"></i> Feltöltés</button>
								</span>
							</div>
						</div>
					</form>
			<div class="row text-center text-lg-left">
				<?php

				$galleryPictureQuery = "SELECT * FROM gallery WHERE familyID=".$_SESSION['familyIDSession'];
				$gallerPictureResult = $connection->query($galleryPictureQuery);

				while($galleryPictureRow = $gallerPictureResult->fetch_assoc()){
					echo "
		        <div class='col-lg-3 col-md-4 col-xs-6' style='margin-bottom:2%;'>
		          <a href='#' name='".$galleryPictureRow['pictureID']."' class='d-block mb-4 h-100 galleryPicture' data-toggle='modal' data-target='#galleryPictureModal'>
		            <img class='img-fluid img-thumbnail' style='max-width:400px; max-height:400px; height:100%; width:100%;' src='".$galleryPictureRow['pictureSource']."' alt='Gallery Picture'>
		          </a>
		        </div>
						";
				}
				?>
      </div>
		</div>
	</div>

	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="userProfileDatas">
		<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
						<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
						<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
						<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
														<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
														<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
														<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
														<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
														<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
														<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
													</select></br>
						<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
		</div>
				<div class="modal-footer">
		<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Gallery picture modal -->
<div class="modal fade" id="galleryPictureModal" tabindex="-1" role="dialog" aria-labelledby="galleryPictureModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title"> <i class="fas fa-image"></i> Kép</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="galleryPictureDiv">

				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>
<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script src="../js/galleryScript.js"></script>
</body>
</html>
