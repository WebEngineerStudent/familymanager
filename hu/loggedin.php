<?php
	include 'php/connection.php';
	session_start();


	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query = "SELECT * FROM familytouser INNER JOIN familys ON familytouser.familyID = familys.familyID WHERE userID = ".$_SESSION['userIdSession'];
	$result = $connection->query($query);

	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row = $result->fetch_assoc();

	$_SESSION["familyIDSession"] = $row['familyID'];

	$familyPictureQuery = "SELECT * FROM familys WHERE familyID=".$_SESSION["familyIDSession"];
	$resultFamilyPic = $connection->query($familyPictureQuery);

	$rowFamily;
?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Familys</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
  <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/loggedin.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
         <img class="img-fluid img-thumbnail mx-auto mb-2" style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src="<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align="center"><?php echo $_SESSION["usernameSession"]." (".$_SESSION["firstnameSession"]." ".$_SESSION["lastnameSession"].") - ".$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="loggedin.php"><i class="fas fa-users"></i> Család</a>
          </li>
					<?php
						if(isset($_SESSION["familyIDSession"])){
							echo"
			          <li class='nav-item'>
			            <a class='nav-link' href='routine.php'><i class='fas fa-calendar-alt'></i> Napi rutin</a>
			          </li>
			          <li class='nav-item'>
			            <a class='nav-link' href='programs.php'><i class='fas fa-child'></i> Családi programok</a>
			          </li>
			          <li class='nav-item'>
			            <a class='nav-link' href='exercises.php'><i class='fas fa-list-ul'></i> Feladatok</a>
			          </li>
			          <li class='nav-item'>
			            <a class='nav-link' href='gallery.php'><i class='fas fa-images'></i> Családi galléria</a>
			          </li>
								<li class='nav-item'>
			            <a class='nav-link' href='chatroom.php'><i class='fas fa-comments'></i> Társalgó</a>
			          </li>
							";
						}
					?>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container col-12 col-lg-8" id="contentDiv">
			<?php
				if ($result->num_rows == 0){
					echo "
						<div class='card mb-3 col-10' id='familysContainerDiv'>
							<div class='card-header'>
								<i class='fa fa-fw fa-users'></i> Családok
							</div>
							<div class='container row col-12' id='cardsDiv'>
										<div class='card col-12 col-lg-5' id='createDiv'>
											<img class='card-img-top' style='height:200px; width:200px;' src='../img/createFamilyCard.png' alt='Card image cap'>
											<div class='card-body'>
												<h5 class='card-title'>Család létrehozása</h5>
												<p class='card-text'>Létrehozhatod a családod csoportját.</p>
												<a href='#' class='btn btn-primary' data-toggle='modal' data-target='#createFamilyModal'><i class='fas fa-users-cog'></i> Család létrehozása</a>
											</div>
										</div>
										<div class='card col-12 col-lg-5' id='connectDiv'>
											<img class='card-img-top' style='height:200px; width:200px;' src='../img/connectFamilyCard.png' alt='Card image cap'>
											<div class='card-body'>
												<h5 class='card-title'>Csatlakozás családhoz</h5>
												<p class='card-text'>Csatlakozz hozzá egy már meglévő családhoz.</p>
												<a href='#' class='btn btn-primary' data-toggle='modal' data-target='#connectFamilyModal'><i class='fas fa-plug'></i> Csatlakozás családhoz</a>
											</div>
										</div>
							</div>
						</div>";
				}else{

					echo "
						<div class='card mb-3 col-10' id='familyContainerDiv' style='max-height:700px;'>
							<div class='card-header'>
								<i class='fa fa-fw fa-users'></i> Családok
								<select class='form-control' id='familySelect'>";
									$result = $connection->query($query);
									while($row2 = $result->fetch_assoc()){
										echo "<option value='".$row['familyID']."'>".$row2['familyName']."</option>";
									}
								echo "
								</select>
								<a href='#' class='btn btn-primary' data-toggle='modal' id='connectOtherFamilyButton' data-target='#connectFamilyModal'><i class='fa fa-plug' aria-hidden='true'></i> Csatlakozás másik családhoz</a>
							</div>
							<div class='container row' id='familyDetailsDiv'>";
								echo "
										<div class='col-12 col-lg-8 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
											<a href='#' data-toggle='modal' data-target='#familyImageModal'><img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:200px; max-height:200px; min-width:200px; min-height:200px; height:100%; width:100%; margin:auto;' src='"; $rowFamily = $resultFamilyPic->fetch_assoc(); if($rowFamily['familyPicture'] == ""){echo "https://via.placeholder.com/200";}else{echo $rowFamily['familyPicture'];} echo "' alt='FamilyPicture'></a>
										</div>
										<div class='col-12 col-lg-10' id='familyDatasDiv' style='margin:auto;'>
											<h3 align='center'>".$row['familyName']." - Család</h3>
											<button style='margin:auto;' class='btn btn-danger' id='exitFamilyButtonHu'>"; if($row["adminID"] == $_SESSION["userIdSession"]){echo"<i class='fa fa-trash' aria-hidden='true'></i> Család törlése";}else{echo"<i class='fa fa-sign-out' aria-hidden='true'></i> Kilépés a családból";} echo"</button>
											<strong><p>Tagok:</p></strong>";
											$query2 = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE familyID = ".$_SESSION["familyIDSession"];
											$result2 = $connection->query($query2);
											while($row3 = $result2->fetch_assoc()){
												echo "
												<div class='list-group-item list-group-item-action'>
													<a class='hu familyMember' name='".$row3['Id']."' href='#' data-toggle='modal' data-target='#memberProfileModal'>
													<div class='media'>
														<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($row3['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $row3['profilePicture'];} echo "' alt=''>
														<div class='media-body'>
															<strong>".$row3['Username']." </strong>
															<strong> (".$row3['First_Name']." ".$row3['Last_Name'].")</strong>
															<strong>".$row3['Position']." </strong>
														</div>
													</div>
													</a>
													<div class='routineButtonDiv'>
														<button class='btn memberRoutineButton' href='#' onclick='getMemberRoutine(".$row3['Id'].")' name='".$row3['Id']."' class='btn' data-toggle='modal' data-target='#memberRoutineModal'><i class='fas fa-calendar-alt'></i></button>
													</div>
												</div>
												";
											}
										echo "</div>";
							echo "</div>
						</div>
						";

				}
			?>
		</div>
	</div>

	   <!-- Create family modal -->
  <div class="modal fade" id="createFamilyModal" tabindex="-1" role="dialog" aria-labelledby="createFamilyModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class="fas fa-user-plus"></i> Család létrehozása</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
			<form method="POST" action="php/createFamily.php">
				<fieldset>
					<legend>Név</legend>
					<input onkeyup="validateFamilyname()" type="text" placeholder="Család neve:" class="form-control" name="createFamilyNameInput" id="createFamilyNameInput"></br>
				</fieldset>
				<button id="createFamilyBTN"  type="submit" disabled="disabled" class="btn btn-primary"><i class="fas fa-user-plus"></i> Létrehozás</button>
			</form>
		  </div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>

		   <!-- Connect to family modal -->
  <div class="modal fade" id="connectFamilyModal" tabindex="-1" role="dialog" aria-labelledby="connectFamilyModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-plug'></i> Csatlakozás családhoz</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
			<form method="POST">
				<fieldset>
					<legend>Keresés családnév alapján:</legend>
					<input type="text" placeholder="Család neve:" class="form-control" name="familyNameInput" id="familyNameInput"></br>
				</fieldset>
				<button  type="button" class="btn btn-primary" id="searchButtonHu"><i class="fas fa-search"></i> Keresés</button>
			</form></br></br>
			<div id="foundedList">

			</div>
		  		</div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>

		<!-- User profile modal -->
	<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="userProfileDatas">
					<?php
						echo"
						<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
							<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
							<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
								<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
								<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
							</form>
						</div>
						<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
							<form>
								<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
								<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
								<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
								<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
								<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
																<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
																<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
																<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
																<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
																<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
																<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
																<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
																<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
															</select></br>
								<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
							</form>
						</div>";
					?>
		  </div>
          <div class="modal-footer">
			<button class="btn btn-primary" type="button" id="saveProfileButtonHU"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
            <button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>

	<!-- Memeber profile modal -->
	<div class="modal fade" id="memberProfileModal" tabindex="-1" role="dialog" aria-labelledby="memberProfileModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="memberProfileDatas">

		  		</div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>

	<!-- Family image modal -->
	<div class="modal fade" id="familyImageModal" tabindex="-1" role="dialog" aria-labelledby="familyImageModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class="fa fa-file-image-o" aria-hidden="true"></i> Családi profilkép</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="familyImageeDatas">
						<?php

						echo "
							<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
								<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:200px; max-height:200px; min-width:200px; min-height:200px; height:100%; width:100%; margin:auto;' src='"; if($rowFamily['familyPicture'] == ""){echo "https://via.placeholder.com/200";}else{echo $rowFamily['familyPicture'];} echo "' alt='FamilyPicture'>
								<form action='php/uploadFamilyPic.php' method='POST' enctype='multipart/form-data'>
									<input class='form-control' type='file' id='uploadFamilyPicture' name='uploadFamilyPicture'>
									<button class='btn btn-primary' style='width:100%;' id='changeFamilyPictureButton' >"; if($rowFamily['familyPicture'] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
								</form>
							</div>
							<div class='row text-center text-lg-left' style='margin-top:2%;'>";

								if(isset($_SESSION['familyIDSession'])){
									$galleryPictureQuery = "SELECT * FROM gallery WHERE familyID=".$_SESSION['familyIDSession'];
									$gallerPictureResult = $connection->query($galleryPictureQuery);

									while($galleryPictureRow = $gallerPictureResult->fetch_assoc()){
										echo "
							        <div class='col-lg-3 col-md-4 col-xs-6' style='margin-bottom:2%;'>
							          <a href='#' name='{$galleryPictureRow['pictureID']}' class='d-block mb-4 h-100 familyGalleryPicture'>
							            <img class='img-fluid img-thumbnail' style='max-width:100px; max-height:100px; height:100%; width:100%;' src='{$galleryPictureRow['pictureSource']}' alt='Gallery Picture'>
							          </a>
							        </div>
											";
									}
								}

								echo"
								</div>
							";
						?>
		  		</div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>

		<!-- Memeber routine modal -->
		<div class="modal fade" id="memberRoutineModal" tabindex="-1" role="dialog" aria-labelledby="memberRoutineModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#bd5d38;">
							<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Napi rutin</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body row" id="memberRoutineDatas">

						</div>
						<div class="modal-footer">
							<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
						</div>
					</div>
				</div>
			</div>
<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script src="../js/loggedinScript.js"></script>
</body>
</html>
