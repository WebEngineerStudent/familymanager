<?php

  include 'connection.php';
  session_start();

  $onlinesQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE Online = 1 AND familyID = ".$_SESSION["familyIDSession"];
  $onlinesResult = $connection->query($onlinesQuery);
  while($onlinesRow = $onlinesResult->fetch_assoc()){
    echo "<a class='list-group-item list-group-item-action hu onlinemember' name='".$onlinesRow['Id']."' href='#' data-toggle='modal' data-target='#memberProfileModal'>
    <div class='media'>
      <img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($onlinesRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $onlinesRow['profilePicture'];} echo "' alt=''>
      <div class='media-body'>
        <strong>".$onlinesRow['Username']." </strong>
        <strong> (".$onlinesRow['First_Name']." ".$onlinesRow['Last_Name'].")</strong>
      </div>
    </div>
    </a>";
  }
