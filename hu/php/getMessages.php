<?php

  include 'connection.php';
  session_start();

  $messagesQuery = "SELECT * FROM chat INNER JOIN users ON chat.userID = users.Id WHERE chat.familyID = ".$_SESSION["familyIDSession"]." ORDER BY messageID";
  $messagesResult = $connection->query($messagesQuery);
  while($messagesRow = $messagesResult->fetch_assoc()){
    echo "<a class='list-group-item list-group-item-action hu message'  name='".$messagesRow['messageID']."'>
    <div class='media'>
      <img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($messagesRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $messagesRow['profilePicture'];} echo "' alt=''>
      <div class='media-body'>
        <strong>".$messagesRow['Username']." : </strong>
        <div class='messageContainerDiv'>
          <strong><p style='word-break: break-all;'>".$messagesRow['message']."</p></strong><br><br>
        </div>
        <p style='font-size:0.7em;'>".$messagesRow['sentDate']."</p>
      </div>
    </div>
    </a>";
  }
