<?php

	include 'connection.php';
	session_start();

	$userID = $_POST['userID'];

	$sql = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN familys ON familytouser.familyID = familys.familyID WHERE Id =".$userID;
	$result = $connection->query($sql);

	if($result->num_rows == 1){
		$row = $result->fetch_assoc();

		echo "
			<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
				<img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($row['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row['profilePicture'];} echo "' alt='ProfilePicture'>
			</div><br>";
			if($row['adminID'] == $_SESSION["userIdSession"] && $row['Id'] != $_SESSION["userIdSession"]){
				echo "<div style='width:100%;'><button style='margin-left:25%;' class='btn btn-danger kickbutton' onclick='kickUser(".$row['Id'].")' name='".$row['Id']."'><i class='fas fa-user-slash'></i> Eltávolítás a családból </button></div>";
			}
			echo "
			<br><div class='col-12 col-lg-10' id='familyDatasDiv' style='margin:auto;'>
				<form>
					<label>Felhasználónév:</label> <span>".$row['Username']."</span></br>
					<label>Vezetéknév:</label> <span>".$row['First_Name']."</span></br>
					<label>Keresztnév:</label> <span>".$row['Last_Name']."</span></br>
					<label>Email:</label> <span>".$row['Email']."</span></br>
					<label>Szerepkör:</label> <span>".$row['Position']."</span></br>
					<label>Elérhető:</label> <span>"; if($row['Online'] == 0){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
				</form>
			</div>
		";

	}
