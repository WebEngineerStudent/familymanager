<?php

	include 'connection.php';
	session_start();

	$target_dir = "../../img/galeryPictures/";
	$target_file = $target_dir . basename($_FILES["uploadGaleryPicture"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["uploadGaleryPicture"]["tmp_name"]);
		if($check !== false) {
			echo "A fájl egy kép  - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			echo "A fájl nem kép.";
			$uploadOk = 0;
		}
	}

	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Ez a fájl már létezik.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["uploadGaleryPicture"]["size"] > 500000) {
		echo "Túl nagy a file.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		echo "Csak JPG, JPEG, PNG & GIF formátumok vannak engedélyezve.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "A fájl nem lett feltöltve.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["uploadGaleryPicture"]["tmp_name"], $target_file)) {
			echo "A file ". basename( $_FILES["uploadGaleryPicture"]["name"]). " feltöltve.";
			$sql = "INSERT INTO gallery (userID, familyID, pictureSource) VALUES ({$_SESSION['userIdSession']}, {$_SESSION['familyIDSession']}, '../img/galeryPictures/". basename($_FILES['uploadGaleryPicture']['name'])."')";
			$result = $connection->query($sql);
			header("location:../gallery.php");
		} else {
			echo "Hiba történt a file feltöltése közben.";
		}
	}
