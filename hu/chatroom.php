<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$userdatasQuery = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$userdatasResult = $connection->query($userdatasQuery);

	$userDatasRow = $userdatasResult->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Társalgó</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
  <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href="../css/resume.css" rel="stylesheet">
  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/chatroom.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src="<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align='center'><?php echo $_SESSION['usernameSession'].' ('.$_SESSION['firstnameSession'].' '.$_SESSION['lastnameSession'].') - '.$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Család</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Napi rutin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="programs.php"><i class="fas fa-child"></i> Családi programok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Feladatok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Családi galléria</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link active" href="chatroom.php"><i class="fas fa-comments"></i> Társalgó</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container row col-12 col-lg-8" id="contentDiv">
			<div class="card mb-3 col-10 col-lg-7" id="chatContainerDiv">
				<div class="card-header">
					<i class="fas fa-comments"></i> Chat
				</div>
				<div class="container" id="messagesDiv">
					<?php
						$messagesQuery = "SELECT * FROM chat INNER JOIN users ON chat.userID = users.Id WHERE chat.familyID = ".$_SESSION['familyIDSession']." ORDER BY messageID";
						$messagesResult = $connection->query($messagesQuery);
						while($messagesRow = $messagesResult->fetch_assoc()){
							echo "<a class='list-group-item list-group-item-action hu message'  name='".$messagesRow['messageID']."'>
							<div class='media'>
								<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($messagesRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $messagesRow['profilePicture'];} echo "' alt=''>
								<div class='media-body'>
									<strong>".$messagesRow['Username']." : </strong>
									<div class='messageContainerDiv'>
										<strong><p style='word-break: break-all;'>".$messagesRow['message']."</p></strong><br><br>
									</div>
									<p style='font-size:0.7em;'>".$messagesRow['sentDate']."</p>
								</div>
							</div>
							</a>";
						}
					?>
				</div>
				<div class="container" id="sendMessagesDiv">
					<form action="php/sendMessage.php" method="post" style="width:100%; height:100%;">
						<textarea type="text" style="resize:none;" name="message" placeholder="Üzenet:" class="form-control" id="messageInput"></textarea>
						<button type="button" class="btn btn-primary" id="sendMessageButton"><i class="fas fa-share"></i> Küldés</button>
					</form>
				</div>
			</div>
			<div class="card mb-3 col-10 col-lg-4" id="onlinesContainerDiv">
				<div class="card-header">
					<i class="fas fa-users"></i> Elérhető:
				</div>
				<div class="container" id="onlinersDiv">
					<?php
						$onlinesQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE Online = 1 AND familyID = ".$_SESSION["familyIDSession"];
						$onlinesResult = $connection->query($onlinesQuery);
						while($onlinesRow = $onlinesResult->fetch_assoc()){
							echo "<a class='list-group-item list-group-item-action hu onlinemember' name='".$onlinesRow['Id']."' href='#' data-toggle='modal' data-target='#memberProfileModal'>
							<div class='media'>
								<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($onlinesRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $onlinesRow['profilePicture'];} echo "' alt=''>
								<div class='media-body'>
									<strong>".$onlinesRow['Username']." </strong>
									<strong> (".$onlinesRow['First_Name']." ".$onlinesRow['Last_Name'].")</strong>
								</div>
							</div>
							</a>";
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="userProfileDatas">
		<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
						<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
						<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
						<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
														<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
														<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
														<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
														<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
														<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
														<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
													</select></br>
						<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
		</div>
				<div class="modal-footer">
		<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Memeber profile modal -->
	<div class="modal fade" id="memberProfileModal" tabindex="-1" role="dialog" aria-labelledby="memberProfileModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color:#bd5d38;">
						<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body row" id="memberProfileDatas">

					</div>
					<div class="modal-footer">
						<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
					</div>
				</div>
			</div>
		</div>

<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script src="../js/chatroomScript.js"></script>
</body>
</html>
