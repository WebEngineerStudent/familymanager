<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION['isLoggedInSession']){
		header('location:index.php');
	}

	$sql = 'SELECT userID, exerciseName, exerciseCell FROM exercises WHERE userID ='.$_SESSION['userIdSession'];
	$result = $connection->query($sql);

	$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);


	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();
?>

<!DOCTYPE html>
<html lang='hu'>
<head>
  <!-- Required metas -->
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
  <title>Family Manager - Routine</title>
  <link rel='shortcut icon' type='image/x-icon' href='../img/icon.png' />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
  <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href='../css/resume.css' rel='stylesheet'>
	  <!-- Logged in page style css -->
  <link rel='stylesheet' type='text/css' href='../css/routine.css'/>
</head>
<body>
<!-- Main container div -->
	<div class='container-fluid row' id='mainContainerDiv'>
		<!-- Menu div -->
		<nav class='navbar navbar-expand-lg navbar-dark bg-primary fixed-top' id='sideNav'>
		<a class='navbar-brand' style='color:white;'><i class='fas fa-users'></i> Family Manager</a>
			<a class='navbar-brand' href='#page-top' data-toggle='modal' data-target='#userProfileModal'>
        <span class='d-none d-lg-block'>
          <img class='img-fluid img-thumbnail mx-auto mb-2' style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src='<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} ?>' alt='ProfilePicture'>
        </span>
      </a>
			<span class='d-none d-lg-block'>
			<div id='profDetailsDiv'>
				<p align='center'><?php echo $_SESSION['usernameSession'].' ('.$_SESSION['firstnameSession'].' '.$_SESSION['lastnameSession'].') - '.$_SESSION["positionSession"] ?></p>
				<p align='center'><?php echo $_SESSION['emailSession'] ?></p>
			</div>
			</span>
			<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
        <span class='navbar-toggler-icon'></span>
      </button>
      <div class='collapse navbar-collapse' id='navbarSupportedContent'>
        <ul class='navbar-nav'>
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link' href='loggedin.php'><i class='fas fa-users'></i> Család</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link active' href='routine.php'><i class='fas fa-calendar-alt'></i> Napi rutin</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link' href='programs.php'><i class='fas fa-child'></i> Családi programok</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link' href='exercises.php'><i class='fas fa-list-ul'></i> Feladatok</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link' href='gallery.php'><i class='fas fa-images'></i> Családi galléria</a>
          </li>
					<li class='nav-item'>
            <a class='nav-link' href='chatroom.php'><i class='fas fa-comments'></i> Társalgó</a>
          </li>
          <li class='nav-item'>
            <a class='nav-link' href='php/logout.php'><i class='fas fa-sign-out-alt'></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class='container  col-12 col-lg-8' id='contentDiv'>
			<div class='card mb-3 col-12 col-lg-12' id="cardHolder">
				<div class='card-header'>
					<i class='fa fa-fw fa-users'></i> Napi rutin
				</div>
				<div class='container col-12' id='cardsDiv'>
					<div class='table-responsive' id='tableDiv'>
						<table class='table table-hover table-bordered table-condensed table-striped' style='height:95%; margin-top:1%;'>
							<thead>
								<tr>
									<th>Napszak</th>
									<th>Hétfő</th>
									<th>Kedd</th>
									<th>Szerda</th>
									<th>Csütörtök</th>
									<th>Péntek</th>
									<th>Szombat</th>
									<th>Vasárnap</th>
								</tr>
							</thead>
							<tbody>
										<tr>
											<td>Reggel</td>
											<td><div class='row_data' edit_type='click' col_name='mondayMorning'><?php while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayMorning'){ echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayMorning'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayMorning'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayMorning'><?php $result = $connection->query($sql); while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayMorning'){ echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayMorning'><?php $result = $connection->query($sql); while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayMorning'){ echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayMorning'><?php $result = $connection->query($sql); while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayMorning'){ echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayMorning'><?php $result = $connection->query($sql); while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayMorning'){ echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayMorning'><?php $result = $connection->query($sql); while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayMorning'){ echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Délelőtt</td>
											<td><div class='row_data' edit_type='click' col_name='mondayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayForenoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayForenoon'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Ebédidő</td>
											<td><div class='row_data' edit_type='click' col_name='mondayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayNoon'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Kora délután</td>
											<td><div class='row_data' edit_type='click' col_name='mondayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayEAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayEAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Késő délután</td>
											<td><div class='row_data' edit_type='click' col_name='mondayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayLAfterNoon'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayLAfterNoon'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Este</td>
											<td><div class='row_data' edit_type='click' col_name='mondayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayEvening'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayEvening'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayEvening'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
										<tr>
											<td>Éjszaka</td>
											<td><div class='row_data' edit_type='click' col_name='mondayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'mondayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='tuesdayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'tuesdayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='wednesdayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'wednesdayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='thursdayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'thursdayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='fridayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'fridayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='saturdayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'saturdayNight'){  echo $row['exerciseName'];}} ?></div></td>
											<td><div class='row_data' edit_type='click' col_name='sundayNight'><?php $result = $connection->query($sql);  while($row = $result->fetch_assoc()){if($row['exerciseCell'] == 'sundayNight'){  echo $row['exerciseName'];}} ?></div></td>
										</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="userProfileDatas">
		<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
						<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
						<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
						<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
														<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
														<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
														<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
														<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
														<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
														<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
													</select></br>
						<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
		</div>
				<div class="modal-footer">
		<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>
<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script src='../js/routine.js'></script>
</body>
</html>
