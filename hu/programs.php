<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$userdatasQuery = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$userdatasResult = $connection->query($userdatasQuery);

	$userDatasRow = $userdatasResult->fetch_assoc();

	$userLikesQuery = "SELECT * FROM likes WHERE userID = ".$_SESSION['userIdSession'];
	$userLikesResult = $connection->query($userLikesQuery);

?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Programok</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/programs.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src="<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align='center'><?php echo $_SESSION['usernameSession'].' ('.$_SESSION['firstnameSession'].' '.$_SESSION['lastnameSession'].') - '.$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Család</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Napi rutin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="programs.php"><i class="fas fa-child"></i> Családi programok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Feladatok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Családi galléria</a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="chatroom.php"><i class="fas fa-comments"></i> Társalgó</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container col-12 col-lg-8" id="contentDiv">
			<!-- Make programs div -->
			<div class="card mb-3 col-lg-12 col-10" id="programMakerDiv">
				<div class="card-header">
					<i class="fa fa-fw fa-blind"></i> Családi program létrehozása</div>
						<div class="container">
								<div class="table-responsive">
									<form action="php/createProgram.php" method="post">
										<table class="table table-striped table-hover">
											<tbody>
												<tr>
													<td><input type="text" id="programName" name="programName" class="form-control" placeholder="Program neve:"/></td>
													<td><input type="date" id="programDate" name="programDate" class="form-control"/></td>
													<td><textarea type="text" id="programDescription" name="programDescription" style="resize:none;" class="form-control" placeholder="Program leírása:"></textarea></td>
													<td><button type="submit" id="createProgram" class="btn btn-primary"><i class="fas fa-plus"></i> Létrehoz</button></td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
					<!-- Programs div -->
				<div class="card mb-3 col-lg-12 col-10" id="familyProgramsDiv">
					<div class="card-header"><i class="fa fa-fw fa-bus"></i> Családi programok</div>
					<div id="programsList" class="list-group list-group-flush small scroll">
						<?php

								$getProgramDatasQuery = "SELECT * FROM programs WHERE familyID =".$_SESSION["familyIDSession"];
								$getProgramDatasResult = $connection->query($getProgramDatasQuery);

								if ($getProgramDatasResult->num_rows > 0) {
									while($getProgramDatasRow = $getProgramDatasResult->fetch_assoc()) {
										$otherMemberQuery = "SELECT Id, Username, profilePicture FROM users WHERE Id=".$getProgramDatasRow['userID'];
										$otherMemberResult = $connection->query($otherMemberQuery);
										$otherMemberRow = $otherMemberResult->fetch_assoc();
										$memberProgramsQuery = "SELECT * FROM likes WHERE programID = ".$getProgramDatasRow['programID'];
										$memberProgramsResult = $connection->query($memberProgramsQuery);
										$likes = 0;
										$dislikes = 0;
										while($rowLikeNumbers = $memberProgramsResult->fetch_assoc()){
											if($rowLikeNumbers['liked']){
												$likes++;
											}else{
												$dislikes++;
											}
										}
										echo "
											<div class='list-group-item list-group-item-action'>
												<a class='program' name='".$getProgramDatasRow['programID']."' href='#' data-toggle='modal' data-target='#memberProgramModal'>
													<div class='media'>
														<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($otherMemberRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $otherMemberRow['profilePicture'];} echo "' alt=''>
														<div class='media-body'>
															<strong>".$otherMemberRow['Username']." </strong>közzétett egy programot:
															<strong> ".$getProgramDatasRow['programName']."</strong>.
															<div class='text-muted smaller'>".$getProgramDatasRow['programDate']."</div>
														</div>
													</div>
												</a>
												<div class='voteButtonsDiv' style='z-index:10;'>
													<button class='btn btn-primary'";$memberProgramsResult = $connection->query($memberProgramsQuery); while($rowLike = $memberProgramsResult->fetch_assoc()){if($rowLike['liked'] == 1 && $rowLike['userID'] == $_SESSION['userIdSession']){echo "disabled = 'disabled'";}}  echo "onclick='like(true, ".$getProgramDatasRow['programID'].")'><i class='fa fa-thumbs-up' aria-hidden='true'></i> Tetszik $likes</button>
													<button class='btn btn-danger'";$memberProgramsResult = $connection->query($memberProgramsQuery); while($rowLike = $memberProgramsResult->fetch_assoc()){if($rowLike['liked'] == 0 && $rowLike['userID'] == $_SESSION['userIdSession']){echo "disabled = 'disabled'";}} echo "onclick='like(false, ".$getProgramDatasRow['programID'].")'><i class='fa fa-thumbs-down' aria-hidden='true'></i> Nem tetszik $dislikes</button>
												</div>
											</div>";
									}
								}
						?>
					</div>
				</div>
		</div>
	</div>


	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="userProfileDatas">
		<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
						<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
						<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
						<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
														<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
														<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
														<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
														<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
														<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
														<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
													</select></br>
						<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
		</div>
				<div class="modal-footer">
		<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Show exercise modal -->
	<div class="modal fade" id="memberProgramModal" tabindex="-1" role="dialog" aria-labelledby="memberProgramModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color:#bd5d38;">
						<h5 class="modal-title"> <i class='fas fa-share'></i> Feladat:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body" id="memberProgramDatas">

					</div>
					<div class="modal-footer">
						<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
					</div>
				</div>
			</div>
		</div>

<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script src="../js/programs.js"></script>
</body>
</html>
