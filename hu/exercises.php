<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query2 = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE familytouser.familyID = ".$_SESSION['familyIDSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();

	$getPointsQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE familytouser.familyID = ".$_SESSION['familyIDSession']." AND users.Id =".$_SESSION["userIdSession"];
	$getPointsResult = $connection->query($getPointsQuery);
	$getPointsRow = $getPointsResult->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Feladatok</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" type="text/css" href="../css/fontawesome/css/all.css">
  <!-- Bootstrap css -->
  <link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/exercises.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" style="max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%;" src="<?php if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION['profilePictureSession'];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align="center"><?php echo $_SESSION["usernameSession"]." (".$_SESSION["firstnameSession"]." ".$_SESSION["lastnameSession"].") - ".$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
					<li class="nav-item" id="profileMenuItem">
            <a class="nav-link" href="#" data-toggle='modal' data-target='#userProfileModal'><i class="fas fa-user"></i> Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Család</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Napi rutin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="programs.php"><i class="fas fa-child"></i> Családi programok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="exercises.php"><i class="fas fa-list-ul"></i> Feladatok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Családi galléria</a>
          </li>
			<li class="nav-item">
            <a class="nav-link" href="chatroom.php"><i class="fas fa-comments"></i> Társalgó</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Kijelentkezés</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
	<div class="container col-12 col-lg-8" id="contentDiv">
			<!-- Points div -->
			<?php
			if($_SESSION["positionSession"] == "Húg" || $_SESSION["positionSession"] == "Bátyj" || $_SESSION["positionSession"] == "Brother" || $_SESSION["positionSession"] == "Sister"){
				echo"
				<div class='card mb-3 col-lg-10 col-10' id='pointsDiv'>
					<div class='card-header'><i class='fas fa-star'></i> Szorgalmad</div>
					<div id='pointsArena' class='list-group list-group-flush small scroll table-responsive'>
								<table class='table table-striped table-hover'>
									<thead>
										<tr>
											<th>Pontjaid:</th>
											<th>Passzok:</th>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<tr>";
											if($row4['Points'] >= 10){
												$pointChangeQuery = "UPDATE users SET Points -= 10, Passes += 1 WHERE Id=".$_SESSION['userIdSession'];
												$connection->query($pointChangeQuery);

												echo "
												<td><p>".$getPointsRow['Points']."</p></td>
												<td><p>".$getPointsRow['Passes']."</p></td>";
											}else{
											echo "
												<td><p>".$getPointsRow['Points']."</p></td>
												<td><p>".$getPointsRow['Passes']."</p></td>";
											}
										echo "
										</tr>
									</tbody>
								</table>
					</div>
				</div>
			<div class='col-12 col-lg-12 row' id='cardsHolderDiv'>
				<!-- To do exercises div -->
				<div class='card mb-3 col-10 col-lg-5' id='toDoExercisesDiv'>
					<div class='card-header'><i class='fas fa-list-alt'></i> Elvégzendő feladatok</div>
					<div id='exerciseList' class='list-group list-group-flush small scroll'>";
						$doExercisesChildQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN todolist ON todolist.toUserID = users.Id WHERE familyID = ".$_SESSION['familyIDSession']." AND isDone = 0 AND Id =".$_SESSION['userIdSession'];
						$doExercisesChildResult = $connection->query($doExercisesChildQuery);

						while($doExercisesChildRow = $doExercisesChildResult->fetch_assoc()){
							echo "
							<div class='list-group-item list-group-item-action'>
									<a class='hu exercise' name='".$doExercisesChildRow['exerciseID']."' href='#' data-toggle='modal' data-target='#memberExerciseModal'>
									<div class='media'>
										<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($doExercisesChildRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $doExercisesChildRow['profilePicture'];} echo "' alt=''>
										<div class='media-body'>
											<strong>".$doExercisesChildRow['Username']." - </strong>
											<strong> ".$doExercisesChildRow['exerciseName']." </strong>
											<strong>".$doExercisesChildRow['deadLineDate']." </strong>
										</div>

									</div>
									</a>
									<div class='doneButtonDiv' style='z-index:10;'>
										<button class='btn btn-primary finishButton' id='".$doExercisesChildRow['exerciseID']."' style='margin-top:1%; width:45%;' ><i class='fas fa-check-circle'></i> Kész </button>
										<button class='btn btn-warning passButton' style='margin-top:1%; width:45%;' data-toggle='modal' data-id='".$doExercisesChildRow['exerciseID']."' data-target='#passToUserModal'"; if($doExercisesChildRow['Passes'] < 1){echo "disabled='disabled'";}  echo "><i class='fas fa-share'></i> Passz </button>
									</div>
								</div>
							";
						}
					echo "
					</div>
				</div>
				<!-- Done exercises div -->
				<div class='card mb-3 col-10 col-lg-5' id='doneExercisesDiv'>
					<div class='card-header'><i class='fas fa-clipboard-check'></i> Elvégzett feladatok</div>
					<div id='doneExercisesList' class='list-group list-group-flush small scroll'>";
						$doneExercisesChildQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN todolist ON todolist.toUserID = users.Id WHERE familyID = ".$_SESSION['familyIDSession']." AND isDone = 1 AND Id =".$_SESSION['userIdSession'];
						$doneExercisesChildResult = $connection->query($doneExercisesChildQuery);

						while($doneExercisesChildRow = $doneExercisesChildResult->fetch_assoc()){
							echo "
								<a class='list-group-item list-group-item-action hu exercise' name='".$doneExercisesChildRow['exerciseID']."' href='#' data-toggle='modal' data-target='#memberExerciseModal'>
								<div class='media'>
									<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($doneExercisesChildRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $doneExercisesChildRow['profilePicture'];} echo "' alt=''>
									<div class='media-body'>
										<strong>".$doneExercisesChildRow['Username']." - </strong>
										<strong> ".$doneExercisesChildRow['exerciseName']." </strong>
										<strong>".$doneExercisesChildRow['deadLineDate']." </strong>
									</div>
								</div>
								</a>
							";
						}
					echo "
					</div>
				</div>
			</div>
		";}else{
			echo "
			<div class='card mb-3 col-lg-10 col-10' id='giveExerciseDiv'>
				<div class='card-header'><i class='fas fa-star'></i> Feladat kiosztása</div>
				<div id='pointsArena' class='list-group list-group-flush small scroll table-responsive'>
							<table class='table table-striped table-hover'>
								<thead>
									<tr>
										<th>Célszemély:</th>
										<th>Feladat neve:</th>
										<th>Leírás:</th>
										<th>Határidő:</th>
										<td></td>
									</tr>
								</thead>
								<tbody>
									<form action='php/giveExercise.php' method='POST'>
										<tr>
											<td><select class='form-control' name='toMembersList' id='toMembersList'>";$result3 = $connection->query($query2); while($row5 = $result3->fetch_assoc()){if($row5['Id'] != $_SESSION['userIdSession']){echo "<option value='".$row5['Id']."'>".$row5['First_Name']." ".$row5['Last_Name']."</option>";}} echo "</select></td>
											<td><input type='text' class='form-control' name='toDoName' id='toDoName' ></td>
											<td><textarea type='text' style='resize:none;' class='form-control' name='toDoDescription' id='toDoDescription'></textarea></td>
											<td><input type='date' class='form-control' name='toDoEndDate' id='toDoEndDate' ></td>
											<td><button class='btn btn-primary' type='submit' id='giveExerciseButton'>Kioszt!</button></td>
										</tr>
									</form>
								</tbody>
							</table>
				</div>
			</div>
		<div class='col-12 col-lg-12 row' id='cardsHolderDiv'>
			<!-- To do exercises div -->
			<div class='card mb-3 col-10 col-lg-5' id='toDoExercisesDiv'>
				<div class='card-header'><i class='fas fa-list-alt'></i> Elvégzendő feladatok</div>
				<div id='exerciseList' class='list-group list-group-flush small scroll'>";
					$doExercisesParentQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN todolist ON todolist.toUserID = users.Id WHERE familyID = ".$_SESSION['familyIDSession']." AND isDone = 0";
					$doExercisesParentResult = $connection->query($doExercisesParentQuery);

					while($doExercisesParentRow = $doExercisesParentResult->fetch_assoc()){
						echo "
							<a class='list-group-item list-group-item-action hu exercise' name='".$doExercisesParentRow['exerciseID']."' href='#' data-toggle='modal' data-target='#memberExerciseModal'>
							<div class='media'>
								<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($doExercisesParentRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $doExercisesParentRow['profilePicture'];} echo "' alt=''>
								<div class='media-body'>
									<strong>".$doExercisesParentRow['Username']." </strong>
									<strong> ".$doExercisesParentRow['exerciseName']." </strong>
									<strong>".$doExercisesParentRow['deadLineDate']." </strong>
								</div>
							</div>
							</a>
						";
					}
				echo "
				</div>
			</div>
			<!-- Done exercises div -->
			<div class='card mb-3 col-10 col-lg-5' id='doneExercisesDiv'>
				<div class='card-header'><i class='fas fa-clipboard-check'></i> Elvégzett feladatok</div>
				<div id='doneExercisesList' class='list-group list-group-flush small scroll'>";
					$doneExercisesParentQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN todolist ON todolist.toUserID = users.Id WHERE familyID = ".$_SESSION['familyIDSession']." AND isDone = 1";
					$doneExercisesParentResult = $connection->query($doneExercisesParentQuery);

					while($doneExercisesParentRow = $doneExercisesParentResult->fetch_assoc()){
						echo "
							<a class='list-group-item list-group-item-action hu exercise' name='".$doneExercisesParentRow['exerciseID']."' href='#' data-toggle='modal' data-target='#memberExerciseModal'>
							<div class='media'>
								<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($doneExercisesParentRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $doneExercisesParentRow['profilePicture'];} echo "' alt=''>
								<div class='media-body'>
									<strong>".$doneExercisesParentRow['Username']." </strong>
									<strong> ".$doneExercisesParentRow['exerciseName']." </strong>
									<strong>".$doneExercisesParentRow['deadLineDate']." </strong>
								</div>
							</div>
							</a>
						";
					}
				echo "
				</div>
			</div>
		</div>
			";
		}
		?>
	</div>
</div>

<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#bd5d38;">
				<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profil</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body row" id="userProfileDatas">
				<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($_SESSION["profilePictureSession"] == ""){echo "https://via.placeholder.com/150";}else{echo $_SESSION["profilePictureSession"];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input class='form-control' type='file' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($_SESSION["profilePictureSession"] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Felhasználónév:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$_SESSION["usernameSession"]."'/></br>
						<label>Keresztnév:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$_SESSION["firstnameSession"]."'/></br>
						<label>Vezetéknév:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$_SESSION["lastnameSession"]."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$_SESSION["emailSession"]."'/></br>
						<label>Szerepkör:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
														<option "; if($_SESSION["positionSession"] == "Apa"){echo "selected";} echo ">Apa</option>
														<option "; if($_SESSION["positionSession"] == "Anya"){echo "selected";} echo ">Anya</option>
														<option "; if($_SESSION["positionSession"] == "Nagyapa"){echo "selected";} echo ">Nagyapa</option>
														<option "; if($_SESSION["positionSession"] == "Nagymama"){echo "selected";} echo ">Nagymama</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőapa"){echo "selected";} echo ">Nevelőapa</option>
														<option "; if($_SESSION["positionSession"] == "Nevelőanya"){echo "selected";} echo ">Nevelőanya</option>
														<option "; if($_SESSION["positionSession"] == "Húg"){echo "selected";} echo ">Húg</option>
														<option "; if($_SESSION["positionSession"] == "Bátyj"){echo "selected";} echo ">Bátyj</option>
													</select></br>
						<label>Elérhető:</label> <span>"; if($_SESSION["isLoggedInSession"] == false){echo "<strong style='color:red;'>Nem elérhető</strong>";}else{echo "<strong style='color:green;'>Elérhető</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Mentés</button>
				<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
			</div>
		</div>
	</div>
</div>

<!-- Pass exercise modal -->
<div class="modal fade" id="passToUserModal" tabindex="-1" role="dialog" aria-labelledby="passToUserModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#bd5d38;">
					<h5 class="modal-title"> <i class='fas fa-share'></i> Passzolás</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body row" id="passToUserDatas">
					<?php
						$passExerciseQuery = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID LEFT JOIN todolist ON todolist.toUserID = users.Id
																	GROUP BY users.Id
																	HAVING familytouser.familyID = ".$_SESSION['familyIDSession']." AND (users.Position = 'Bátyj' OR users.Position = 'Húg') AND (users.Id != ".$_SESSION['userIdSession'].")";

						$passExerciseResult = $connection->query($passExerciseQuery);

						while($passExerciseRow = $passExerciseResult->fetch_assoc()){
							echo "
								<a class='list-group-item list-group-item-action hu passUserItem' href='#' name='".$passExerciseRow['Id']."' >
								<div class='media'>
									<img class='d-flex mr-3 rounded-circle' style='max-width:45px; max-height:45px; min-height:45px; min-width:45px; width:100%; height:100%;' src='"; if($passExerciseRow['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $passExerciseRow['profilePicture'];} echo "' alt=''>
									<div class='media-body'>
										<strong>".$passExerciseRow['Username']." - </strong>
										<strong> (".$passExerciseRow['First_Name']." </strong>
										<strong>".$passExerciseRow['Last_Name'].") </strong>
									</div>
								</div>
								</a>
							";
						}
					?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Show exercise modal -->
	<div class="modal fade" id="memberExerciseModal" tabindex="-1" role="dialog" aria-labelledby="memberExerciseModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color:#bd5d38;">
						<h5 class="modal-title"> <i class='fas fa-share'></i> Feladat:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body" id="memberExerciseDatas">

					</div>
					<div class="modal-footer">
						<button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
					</div>
				</div>
			</div>
		</div>
<!-- Bootstrap js, jquery links -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/pooper.min.js"></script>
<script src="../css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/exerciseScript.js"></script>
</body>
</html>
