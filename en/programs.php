<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Programmes</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
  <!-- Bootstrap css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/programs.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" src="<?php if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align='center'><?php echo $_SESSION['usernameSession'].' ('.$_SESSION['firstnameSession'].' '.$_SESSION['lastnameSession'].') - '.$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Family</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Daily routine</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="programs.php"><i class="fas fa-child"></i> Family programmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Family gallery</a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="chatroom.php"><i class="fas fa-comments"></i> Chatroom</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container col-12 col-lg-8" id="contentDiv">
			<!-- Make programs div -->
			<div class="card mb-3 col-lg-12 col-10" id="programMakerDiv">
				<div class="card-header">
					<i class="fa fa-fw fa-blind"></i> Create a family programme</div>
						<div class="container">
								<div class="table-responsive">
									<form action="php/createProgram.php" method="post">
										<table class="table table-striped table-hover">
											<tbody>
												<tr>
													<td><input type="text" id="programName" name="programName" class="form-control" placeholder="Programme name:"/></td>
													<td><input type="date" id="programDate" name="programDate" class="form-control"/></td>
													<td><input type="text" id="programDescription" name="programDescription" class="form-control" placeholder="Create the programme:"/></td>
													<td><button type="submit" id="createProgram" class="btn btn-primary"><i class="fas fa-plus"></i> Create</button></td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
					<!-- Programs div -->
				<div class="card mb-3 col-lg-12 col-10" id="familyProgramsDiv">
					<div class="card-header"><i class="fa fa-fw fa-bus"></i> Family programmes</div>
					<div id="programsList" class="list-group list-group-flush small scroll">
						<?php

								$sql = "SELECT programID, familyID, userID, programName, programDate, programDescription FROM programs WHERE familyID =".$_SESSION["familyIDSession"];
								$result = $connection->query($sql);

								if ($result->num_rows > 0) {
									while($row = $result->fetch_assoc()) {
										$sql2 = "SELECT Id, Username FROM users WHERE Id=".$row['userID'];
										$result2 = $connection->query($sql2);
										$row2 = $result2->fetch_assoc();
										echo "<a class='list-group-item list-group-item-action' href='#'>
												<div class='media'>
													<img class='d-flex mr-3 rounded-circle' src='http://placehold.it/45x45' alt=''>
													<div class='media-body'>
														<strong>".$row2['Username']." </strong>Posted a programme:
														<strong> ".$row['programName']."</strong>.
														<div class='text-muted smaller'>".$row['programDescription']."</div>
														<div class='text-muted smaller'>".$row['programDate']."</div>
													</div>
													<div id='voteButtonsDiv'>
														<button class='btn btn-primary' id='likeButton' onclick='php/like.php?programID=".$row['programID']."&liked=true'><i class='fa fa-thumbs-up' aria-hidden='true'></i> Like</button>
														<button class='btn btn-danger' id='dislikeButton' onclick='php/like.php?programID=".$row['programID']."&liked=false'><i class='fa fa-thumbs-down' aria-hidden='true'></i> Dislike</button>
													</div>
												</div>
												</a>";
									}
								}
						?>
					</div>
				</div>
		</div>
	</div>


	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#bd5d38;">
				<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profile</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body row" id="userProfileDatas">
	<?php
		echo"
		<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
			<img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} echo "' alt='ProfilePicture'>
			<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($row4['profilePicture'] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Upload";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Change";} echo "
		</div>
		<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
			<form>
				<label>Username:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$row4['Username']."'/></br>
				<label>First name:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$row4['First_Name']."'/></br>
				<label>Last name:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$row4['Last_Name']."'/></br>
				<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$row4['Email']."'/></br>
				<label>Position:</label> <input type='text' class='form-control' id='ownPositionInput' value='".$row4['Position']."'/></br>
				<label>Online:</label> <span>"; if($row4['Online'] == 0){echo "<strong style='color:red;'>Offline</strong>";}else{echo "<strong style='color:green;'>Online</strong>";} echo "</span></br>
			</form>
		</div>";
	?>
	</div>
			<div class="modal-footer">
	<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap js, jquery links -->
<script src='https://code.jquery.com/jquery-3.1.1.min.js'  crossorigin='anonymous'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="../js/programs.js"></script>
</body>
</html>
