<?php
	include 'php/connection.php';
	session_start();


	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query = "SELECT * FROM familytouser INNER JOIN familys ON familytouser.familyID = familys.familyID WHERE userID = ".$_SESSION['userIdSession'];
	$result = $connection->query($query);

	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();
	
	$familyPictureQuery = "SELECT * FROM familys WHERE familyID=".$_SESSION["familyIDSession"];
	$resultFamilyPic = $connection->query($familyPictureQuery);

	$rowFamily = $resultFamilyPic->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Familys</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
  <!-- Bootstrap css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link href="../css/resume.css" rel="stylesheet">
	  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/loggedin.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid row" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" src="<?php if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align="center"><?php echo $_SESSION["usernameSession"]." (".$_SESSION["firstnameSession"]." ".$_SESSION["lastnameSession"].") - ".$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" href="loggedin.php"><i class="fas fa-users"></i> Family</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Daily routine</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="programs.php"><i class="fas fa-child"></i> Family programmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Family gallery</a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="chatroom.php"><i class="fas fa-comments"></i> Chatroom</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container col-12 col-lg-8" id="contentDiv">
			<?php
				if ($result->num_rows == 0){
					echo "
						<div class='card mb-3 col-10' id='familysContainerDiv'>
							<div class='card-header'>
								<i class='fa fa-fw fa-users'></i> Familys
							</div>
							<div class='container row col-12' id='cardsDiv'>
										<div class='card col-12 col-lg-5' id='createDiv'>
											<img class='card-img-top' style='height:200px; width:200px;' src='../img/createFamilyCard.png' alt='Card image cap'>
											<div class='card-body'>
												<h5 class='card-title'>Create family</h5>
												<p class='card-text'>You can create a group for your family.</p>
												<a href='#' class='btn btn-primary' data-toggle='modal' data-target='#createFamilyModal'><i class='fas fa-users-cog'></i> Create family</a>
											</div>
										</div>
										<div class='card col-12 col-lg-5' id='connectDiv'>
											<img class='card-img-top' style='height:200px; width:200px;' src='../img/connectFamilyCard.png' alt='Card image cap'>
											<div class='card-body'>
												<h5 class='card-title'>Connect to family</h5>
												<p class='card-text'>Connect for a premade family group.</p>
												<a href='#' class='btn btn-primary' data-toggle='modal' data-target='#connectFamilyModal'><i class='fas fa-plug'></i> Connect to family</a>
											</div>
										</div>
							</div>
						</div>";
				}else{


					$row = $result->fetch_assoc();

					$_SESSION["familyIDSession"] = $row['familyID'];

					$result = $connection->query($query);

					echo "
						<div class='card mb-3 col-10' id='familyContainerDiv' style='max-height:700px;'>
							<div class='card-header'>
								<i class='fa fa-fw fa-users'></i> Familys
								<select class='form-control' id='familySelect'>";
									while($row2 = $result->fetch_assoc()){
										echo "<option value='".$row['familyID']."'>".$row2['familyName']."</option>";
									}
								echo "
									<a href='#' class='btn btn-primary' data-toggle='modal' data-target='#connectFamilyModal'>Connect to other family..</a>
								</select>
							</div>
							<div class='container row' id='familyDetailsDiv'>";
								echo "
										<div class='col-12 col-lg-8 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
											<a href='#' data-toggle='modal' data-target='#familyImageModal'><img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($rowFamily['familyPicture'] == ""){echo "https://via.placeholder.com/200";}else{echo $rowFamily['familyPicture'];} echo "' alt='FamilyPicture'></a>
										</div>
										<div class='col-12 col-lg-10' id='familyDatasDiv' style='margin:auto;'>
											<h3 align='center'>".$row['familyName']." - Family</h3>
											<button class='btn btn-danger' id='exitFamilyButtonEn'>Exit from family</button>
											<strong><p>Members:</p></strong>";
											$query2 = "SELECT * FROM users INNER JOIN familytouser ON users.Id = familytouser.userID WHERE familyID = ".$_SESSION["familyIDSession"];
											$result2 = $connection->query($query2);
											while($row3 = $result2->fetch_assoc()){
												echo "<a class='list-group-item list-group-item-action en' name='".$row3['Id']."' href='#' data-toggle='modal' data-target='#memberProfileModal'>
												<div class='media'>
													<img class='d-flex mr-3 rounded-circle' style='width:45px; height:45px;' src='"; if($row3['profilePicture'] == ""){echo "https://via.placeholder.com/45";}else{echo $row3['profilePicture'];} echo "' alt=''>
													<div class='media-body'>
														<strong>".$row3['Username']." </strong>
														<strong> (".$row3['First_Name']." ".$row3['Last_Name'].")</strong>
														<strong>".$row3['Position']." </strong>
													</div>
												</div>
												</a>";
											}
										echo "</div>";
							echo "</div>
							</div>
						</div>
						";

				}
			?>
		</div>
	</div>

	   <!-- Create family modal -->
  <div class="modal fade" id="createFamilyModal" tabindex="-1" role="dialog" aria-labelledby="createFamilyModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class="fas fa-user-plus"></i> Create family</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
			<form method="POST" action="php/createFamily.php">
				<fieldset>
					<legend>Name</legend>
					<input onkeyup="validateFamilyname()" type="text" placeholder="Család neve:" class="form-control" name="createFamilyNameInput" id="createFamilyNameInput"/></br>
				</fieldset>
				<button id="createFamilyBTN"  type="submit" disabled="disabled" class="btn btn-primary"><i class="fas fa-user-plus"></i> Create</button>
			</form>
		  </div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
          </div>
        </div>
      </div>
    </div>

		   <!-- Connect to family modal -->
  <div class="modal fade" id="connectFamilyModal" tabindex="-1" role="dialog" aria-labelledby="connectFamilyModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-plug'></i> Connect to family</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
			<form method="POST">
				<fieldset>
					<legend>Search by family name:</legend>
					<input type="text" placeholder="Family name:" class="form-control" name="familyNameInput" id="familyNameInput"/></br>
				</fieldset>
				<button  type="button" class="btn btn-primary" id="searchButtonEn"><i class="fas fa-search"></i> Search</button>
			</form></br></br>
			<div id="foundedList">

			</div>
		  </div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
          </div>
        </div>
      </div>
    </div>

			<!-- User profile modal -->
	<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profile</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="userProfileDatas">
			<?php
				echo"
				<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
					<img class='img-fluid img-thumbnail mx-auto mb-2' style='max-width:150px; max-height:150px; min-width:150px; min-height:150px; height:100%; width:100%; margin:auto;' src='"; if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} echo "' alt='ProfilePicture'>
					<form action='php/uploadProfPic.php' method='POST' enctype='multipart/form-data'>
						<input type='file' class='form-control' id='uploadProfilePicture' name='uploadProfilePicture'>
						<button type='submit' class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($row4['profilePicture'] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Upload";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Change";} echo "</button>
					</form>
				</div>
				<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
					<form>
						<label>Username:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$row4['Username']."'/></br>
						<label>First name:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$row4['First_Name']."'/></br>
						<label>Last name:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$row4['Last_Name']."'/></br>
						<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$row4['Email']."'/></br>
						<label>Position:</label> <select class='form-control' id='ownPositionInput' name='ownPositionInput'>
													<option "; if($row4['Position'] == "Father"){echo "selected";} echo ">Father</option>
													<option "; if($row4['Position'] == "Mother"){echo "selected";} echo ">Mother</option>
													<option "; if($row4['Position'] == "Grandfather"){echo "selected";} echo ">Grandfather</option>
													<option "; if($row4['Position'] == "Grandmother"){echo "selected";} echo ">Grandmother</option>
													<option "; if($row4['Position'] == "Step father"){echo "selected";} echo ">Step father</option>
													<option "; if($row4['Position'] == "Step mother"){echo "selected";} echo ">Step mother</option>
													<option "; if($row4['Position'] == "Sister"){echo "selected";} echo ">Sister</option>
													<option "; if($row4['Position'] == "Brother"){echo "selected";} echo ">Brother</option>
												</select></br>
						<label>Online:</label> <span>"; if($row4['Online'] == 0){echo "<strong style='color:red;'>Offline</strong>";}else{echo "<strong style='color:green;'>Online</strong>";} echo "</span></br>
					</form>
				</div>";
			?>
		  </div>
          <div class="modal-footer">
			<button class="btn btn-primary" type="button" id="saveProfileButtonEN"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
            <button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
          </div>
        </div>
      </div>
    </div>

		<!-- Memeber profile modal -->
	<div class="modal fade" id="memberProfileModal" tabindex="-1" role="dialog" aria-labelledby="memberProfileModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profile</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="memberProfileDatas">

		  </div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
          </div>
        </div>
      </div>
    </div>

	<!-- Family image modal -->
	<div class="modal fade" id="familyImageModal" tabindex="-1" role="dialog" aria-labelledby="familyImageModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#bd5d38;">
            <h5 class="modal-title" id="registerModalLabel"> <i class="fa fa-file-image-o" aria-hidden="true"></i> Family image</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body row" id="familyImageeDatas">
			<?php

				echo "
					<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
						<img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($rowFamily['familyPicture'] == ""){echo "https://via.placeholder.com/200";}else{echo $rowFamily['familyPicture'];} echo "' alt='FamilyPicture'>
						<form action='php/uploadFamilyPic.php' method='POST' enctype='multipart/form-data'>
							<input class='form-control' type='file' id='uploadFamilyPicture' name='uploadFamilyPicture'>
							<button class='btn btn-primary' style='width:100%;' id='changeFamilyPictureButton' >"; if($rowFamily['familyPicture'] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Feltöltése";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Megváltoztatás";} echo "</button>
						</form>
					</div>
					<div class='row text-center text-lg-left' style='margin-top:2%;'>

						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
						<div class='col-lg-3 col-md-4 col-xs-6'>
						  <a href='#' class='d-block mb-4 h-100'>
							<img class='img-fluid img-thumbnail' src='http://placehold.it/400x300' alt=''>
						  </a>
						</div>
					</div>
				";
			?>
		  </div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> Mégse</button>
          </div>
        </div>
      </div>
    </div>
<!-- Bootstrap js, jquery links -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="../js/loggedinScript.js"></script>
</body>
</html>
