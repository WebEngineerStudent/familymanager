<?php
	include 'php/connection.php';
	session_start();

	if(!$_SESSION["isLoggedInSession"]){
		header("location:index.php");
	}

	$query2 = "SELECT * FROM users WHERE Id = ".$_SESSION['userIdSession'];
	$result3 = $connection->query($query2);

	$row4 = $result3->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="hu">
<head>
  <!-- Required metas -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Family Manager - Chatroom</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/icon.png" />
  <!-- Font awesome css link -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
  <!-- Bootstrap css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link href="../css/resume.css" rel="stylesheet">
  <!-- Logged in page style css -->
  <link rel="stylesheet" type="text/css" href="../css/chatroom.css"/>
</head>
<body>
<!-- Main container div -->
	<div class="container-fluid" id="mainContainerDiv">
		<!-- Menu div -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
		<a class="navbar-brand" style="color:white;"><i class='fas fa-users'></i> Family Manager</a>
			<a class="navbar-brand" href="#page-top" data-toggle='modal' data-target='#userProfileModal'>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-thumbnail mx-auto mb-2" src="<?php if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} ?>" alt="ProfilePicture">
        </span>
      </a>
			<span class="d-none d-lg-block">
			<div id="profDetailsDiv">
				<p align='center'><?php echo $_SESSION['usernameSession'].' ('.$_SESSION['firstnameSession'].' '.$_SESSION['lastnameSession'].') - '.$_SESSION["positionSession"] ?></p>
				<p align="center"><?php echo $_SESSION["emailSession"] ?></p>
			</div>
			</span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="loggedin.php"><i class="fas fa-users"></i> Family</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routine.php"><i class="fas fa-calendar-alt"></i> Daily routine</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="programs.php"><i class="fas fa-child"></i> Family programmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php"><i class="fas fa-list-ul"></i> Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php"><i class="fas fa-images"></i> Family gallery</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link active" href="chatroom.php"><i class="fas fa-comments"></i> Chatroom</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="php/logout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>
          </li>
        </ul>
      </div>
    </nav>
		<!-- Main content div -->
		<div class="container" id="contentDiv">
			<div class="container row" id="chatHolderDiv">
				<div class="justify-content-center col-lg-5" id="messagingSection">
						<div class="container-fluid scroll"  id="messagesdiv">

						</div>
						<div class="text-left" style="margin-top: 2%;">
							<p class="lead emoji-picker-container">
								<input type="text" class="form-control" style="display: inline" id="chatmessage" placeholder="Üzenet:" name="message" data-emojiable="true"/>
							</p>
							<button class="btn btn-primary" id="sendButton" style="display: inline" type="submit" onclick="send()">Send</button>
						</div>
				</div>
				<div id="onlineList" class="col-lg-5">
					<h3>Online - <span id="count_user_online"> </span></h3>
					<div class="container scroll" style="overflow-y: scroll; overflow-x: hidden;" id="onlinesdiv">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- User profile modal -->
<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#bd5d38;">
				<h5 class="modal-title" id="registerModalLabel"> <i class='fas fa-user'></i> Profile</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body row" id="userProfileDatas">
	<?php
		echo"
		<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
			<img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($row4['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row4['profilePicture'];} echo "' alt='ProfilePicture'>
			<button class='btn btn-primary' style='width:100%;' id='changeProfilePictureButton' >"; if($row4['profilePicture'] == ""){echo "<i class='fa fa-upload' aria-hidden='true'></i> Upload";}else{echo "<i class='fa fa-file-image-o' aria-hidden='true'></i> Change";} echo "
		</div>
		<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
			<form>
				<label>Username:</label> <input type='text' class='form-control' id='ownUsernameInput' value='".$row4['Username']."'/></br>
				<label>First name:</label> <input type='text' class='form-control' id='ownFirstnameInput' value='".$row4['First_Name']."'/></br>
				<label>Last name:</label> <input type='text' class='form-control' id='ownLastnameInput' value='".$row4['Last_Name']."'/></br>
				<label>Email:</label> <input type='text' class='form-control' id='ownEmailInput' value='".$row4['Email']."'/></br>
				<label>Position:</label> <input type='text' class='form-control' id='ownPositionInput' value='".$row4['Position']."'/></br>
				<label>Online:</label> <span>"; if($row4['Online'] == 0){echo "<strong style='color:red;'>Offline</strong>";}else{echo "<strong style='color:green;'>Online</strong>";} echo "</span></br>
			</form>
		</div>";
	?>
	</div>
			<div class="modal-footer">
	<button class="btn btn-primary" type="button" id="saveProfileButton"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				<button class="btn btn-danger" type="button" data-dismiss="modal"> <i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap js, jquery links -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="js/loggedinScript.js"></script>
<script type="application/javascript">
function send() {
	var message = $("#chatmessage").val();
        $.ajax({
                type: 'post',
                url: 'php/sendMessage.php',
                data:
				{
					message: message
				},
                success: function () {
                 alert("Message has been sent!");
                }
        });
}
</script>
</body>
</html>
