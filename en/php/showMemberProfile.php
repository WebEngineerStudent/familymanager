<?php 

	include 'connection.php';
	session_start();
	
	$userID = $_POST['userID'];
	
	$sql = "SELECT Username, First_Name, Last_Name, Email, Position, profilePicture, Online FROM users WHERE Id =".$userID;
	$result = $connection->query($sql);
	
	if($result->num_rows == 1){
		$row = $result->fetch_assoc();
		
		echo "
			<div class='col-12 col-lg-5 row justify-content-center' id='familyImageDiv' style='margin:auto;'>
				<img class='img-fluid img-thumbnail mx-auto mb-2' style='margin:auto;' src='"; if($row['profilePicture'] == ""){echo "https://via.placeholder.com/150";}else{echo $row['profilePicture'];} echo "' alt='ProfilePicture'>
			</div>
			<div class='col-12 col-lg-5' id='familyDatasDiv' style='margin:auto;'>
				<form>
					<label>Username:</label> <span>".$row['Username']."</span></br>
					<label>First name:</label> <span>".$row['First_Name']."</span></br>
					<label>Last name:</label> <span>".$row['Last_Name']."</span></br>
					<label>Email:</label> <span>".$row['Email']."</span></br>
					<label>Position:</label> <span>".$row['Position']."</span></br>
					<label>Online:</label> <span>"; if($row['Online'] == 0){echo "<strong style='color:red;'>Offline</strong>";}else{echo "<strong style='color:green;'>Online</strong";} echo "</span></br>
				</form>
			</div>
		";
		
	}