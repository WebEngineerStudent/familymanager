$(document).ready(function(){
	$("#messagesDiv").scrollTop($('#messagesDiv')[0].scrollHeight);
	setInterval(function(){
		getMessages();
	},10000);

	function getMessages(){
		$.ajax({
				type: 'POST',
				url: '../hu/php/getMessages.php',
				success: function(response) {
					$('#messagesDiv').html(response);
					$("#messagesDiv").animate({ scrollTop: $('#messagesDiv')[0].scrollHeight}, 1000);
				}
			});
	}

	$("#sendMessageButton").click(function(){

		let chatMessage = $("#messageInput").val();

		$.ajax({
	          type: 'post',
	          url: '../hu/php/sendMessage.php',
	          data:
					{
						chatMessage: chatMessage
					},
	          success: function (response) {
							getMessages();
							$("#messageInput").val("");
	        }
	      });
	});

	setInterval(function(){
		$.ajax({
				type: 'POST',
				url: '../hu/php/getOnliners.php',
				success: function(response) {
					$('#onlinersDiv').html(response);
				}
			});
	},30000);

	$('.onlinemember').click(function(){
		var userID = $(this).attr('name');
		$.ajax({
	          type: 'post',
	          url: '../hu/php/showMemberProfile.php',
	          data:
					{
						userID: userID
					},
	          success: function (response) {
					  $('#memberProfileDatas').html(response);
	        }
	      });
	});
});
