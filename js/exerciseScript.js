var passExerciseID;

$(".passButton").on("click", function(){
	passExerciseID = $(this).data('id');
});

$(".finishButton").on("click", function(){
	var exerciseID = $(this).attr('id');
	$.ajax({
		type: 'POST',
		url: '../hu/php/makeDoneExercise.php',
		data:
		{
			exerciseID : exerciseID
		},
		success: function(response) {
			location.reload();
		}
	});
});

$(".passUserItem").click(function() {
	var userID = $(this).attr('name');
	$.ajax({
		type: 'POST',
		url: '../hu/php/passExercise.php',
		data:
		{
			userID : userID,
			passExerciseID : passExerciseID
		},
		success: function(response) {
			location.reload();
		}
	});
});

$('.exercise').click(function(){
	var exerciseDetailsID = $(this).attr('name');
	$.ajax({
            type: 'post',
            url: '../hu/php/showExerciseDetails.php',
            data:
						{
							exerciseDetailsID: exerciseDetailsID
						},
            success: function (response) {
							$('#memberExerciseDatas').html(response);
            }
        });
});
