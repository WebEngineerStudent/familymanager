function validateFamilyname(){

	let familyname = document.getElementById("createFamilyNameInput").value;


	if(familyname.length > 3){
		if(familyname[0] == familyname[0].toUpperCase()){
			if(familyname.includes(' ')){
				document.getElementById("createFamilyNameInput").style.border = '2px solid red';
				document.getElementById("createFamilyBTN").disabled = true;
			}else{
				document.getElementById("createFamilyNameInput").style.border = '2px solid green';
				document.getElementById("createFamilyBTN").disabled = false;
			}
		}else{
			document.getElementById("createFamilyNameInput").style.border = '2px solid red';
			document.getElementById("createFamilyBTN").disabled = true;
		}
	}else{
		document.getElementById("createFamilyNameInput").style.border = '2px solid red';
		document.getElementById("createFamilyBTN").disabled = true;
	}

}

$('#searchButtonHu').click(function(){
	let familyName = document.getElementById("familyNameInput").value;
	$.ajax({
                type: 'post',
                url: '../hu/php/connectToFamily.php',
                data:
				{
					familyName: familyName
				},
                success: function (response) {
					console.log(response);
					$('#foundedList').html(response);
                }
        });

});

$('#searchButtonEn').click(function(){
	let familyName = document.getElementById("familyNameInput").value;
	$.ajax({
                type: 'post',
                url: '../en/php/connectToFamily.php',
                data:
				{
					familyName: familyName
				},
                success: function (response) {
					$('#foundedList').html(response);
                }
        });

});

$('.familyMember').click(function(){
	var userID = $(this).attr('name');
	$.ajax({
          type: 'post',
          url: '../hu/php/showMemberProfile.php',
          data:
					{
						userID: userID
					},
          success: function (response) {
						$('#memberProfileDatas').html(response);
          }
    });
});


function getMemberRoutine(uID){
	var routineUserID = uID;
	$.ajax({
          type: 'post',
          url: '../hu/php/showMemberRoutine.php',
          data:
					{
						routineUserID: routineUserID
					},
          success: function (response) {
						$('#memberRoutineDatas').html(response);
          }
    });
}

function kickUser(id){
	var toKickUserID = id;
	$.ajax({
          type: 'post',
          url: '../hu/php/kickUserFromFamily.php',
          data:
				{
					toKickUserID: toKickUserID
				},
        success: function (response) {
						location.reload();
        }
    });
	}

$('.en').click(function(){
	var userID = $(this).attr('name');
	$.ajax({
                type: 'post',
                url: '../en/php/showMemberProfile.php',
                data:
				{
					userID: userID
				},
                success: function (response) {
					$('#memberProfileDatas').html(response);
                }
        });

});

$('#exitFamilyButtonEn').click(function(){
	$.ajax({
                type: 'post',
                url: '../en/php/exitFamily.php',
                success: function (response) {
					location.reload();
                }
        });

});

$('#exitFamilyButtonHu').click(function(){
	$.ajax({
                type: 'post',
                url: '../hu/php/exitFamily.php',
                success: function (response) {
					location.reload();
                }
        });

});

$('#saveProfileButtonHU').click(function(){

	let usernameSave = document.getElementById("ownUsernameInput").value;
	let firstnameSave = document.getElementById("ownFirstnameInput").value;
	let lastnameSave = document.getElementById("ownLastnameInput").value;
	let emailSave = document.getElementById("ownEmailInput").value;
	let positionSave = document.getElementById("ownPositionInput").value;

	$.ajax({
                type: 'post',
                url: '../hu/php/saveProfile.php',
				data:
				{
					usernameSave: usernameSave,
					firstnameSave: firstnameSave,
					lastnameSave: lastnameSave,
					emailSave: emailSave,
					positionSave: positionSave
				},
                success: function (response) {
					location.reload();
                }
        });

});

$('#saveProfileButtonEN').click(function(){

	let usernameSave = document.getElementById("ownUsernameInput").value;
	let firstnameSave = document.getElementById("ownFirstnameInput").value;
	let lastnameSave = document.getElementById("ownLastnameInput").value;
	let emailSave = document.getElementById("ownEmailInput").value;
	let positionSave = document.getElementById("ownPositionInput").value;

	$.ajax({
                type: 'post',
                url: '../en/php/saveProfile.php',
				data:
				{
					usernameSave: usernameSave,
					firstnameSave: firstnameSave,
					lastnameSave: lastnameSave,
					emailSave: emailSave,
					positionSave: positionSave
				},
                success: function (response) {
					location.reload();
                }
        });

});

$(".familyGalleryPicture").click(function() {
	var galleryPictureID = $(this).attr('name');
	$.ajax({
		type: 'POST',
		url: '../hu/php/changeFamilyPicture.php',
		data:
		{
			galleryPictureID : galleryPictureID
		},
		success: function(response) {
			location.reload();
		}
	});
});
