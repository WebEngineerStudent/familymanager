$(document).ready(function(){
	setInterval(function(){
		$.ajax({
				type: 'POST',
				url: '../hu/php/getProgram.php',
				success: function(response) {
					$('#programsList').html(response);
				}
			});
	},60000);
});

function like(liked, programID){
	console.log(liked);
	$.ajax({
		type: 'POST',
		url: '../hu/php/like.php',
		data:
		{
			liked : liked,
			programID : programID
		},
		success: function(response) {
			location.reload();
		}
	});
}

$('.program').click(function(){
	var memberProgramID = $(this).attr('name');
	$.ajax({
            type: 'post',
            url: '../hu/php/showProgramDetails.php',
            data:
						{
							memberProgramID: memberProgramID
						},
            success: function (response) {
							$('#memberProgramDatas').html(response);
            }
        });
});
